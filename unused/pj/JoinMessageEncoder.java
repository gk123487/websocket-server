package com.swiggy.pj;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.websocket.*;
import java.io.IOException;

public class JoinMessageEncoder implements Encoder.Text<JoinRequest>, Decoder.Text<JoinRequest> {

    private static ObjectMapper mapper = new ObjectMapper();

    @Override
    public String encode(JoinRequest m) throws EncodeException {
        try {
            return mapper.writeValueAsString(m);
        } catch (JsonProcessingException e) {
            throw new EncodeException(m, "", e);
        }
    }

    @Override
    public void init(EndpointConfig config) {

    }

    @Override
    public void destroy() {

    }

    @Override
    public JoinRequest decode(String s) throws DecodeException {
        try {
            return mapper.readValue(s, JoinRequest.class);
        } catch (IOException e) {
            throw new DecodeException(s, "", e);
        }
    }

    @Override
    public boolean willDecode(String s) {
        return true;
    }
}
