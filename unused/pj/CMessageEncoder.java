package com.swiggy.pj;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.websocket.*;
import java.io.IOException;

public class CMessageEncoder implements Encoder.Text<CMessage>, Decoder.Text<CMessage> {

    private static ObjectMapper mapper = new ObjectMapper();

    @Override
    public String encode(CMessage m) throws EncodeException {
        try {
            return mapper.writeValueAsString(m);
        } catch (JsonProcessingException e) {
            throw new EncodeException(m, "", e);
        }
    }

    @Override
    public void init(EndpointConfig config) {

    }

    @Override
    public void destroy() {

    }

    @Override
    public CMessage decode(String s) throws DecodeException {
        try {
            return mapper.readValue(s, CMessage.class);
        } catch (IOException e) {
            throw new DecodeException(s, "", e);
        }
    }

    @Override
    public boolean willDecode(String s) {
        return true;
    }
}
