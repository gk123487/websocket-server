package com.swiggy.pj;

import javax.websocket.Session;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class SessionHolders {

    private Map<String, Session> sessionMap = new HashMap<>();

    public Optional<Session> get(String userId, String roomId) {
        return Optional.of(sessionMap.get(join(userId, roomId)));
    }

    public void put(String userId, String roomId, Session s) {
        this.sessionMap.put(join(userId, roomId), s);
    }

    private String join(String userId, String roomId) {
        return userId + "_" + roomId;
    }
}
