package com.swiggy.pj;

import lombok.Data;

@Data
public class CMessage {

    public CMessage() {
    }

    public CMessage(String msg) {
        this.msg = msg;
    }

    private String msg;

}