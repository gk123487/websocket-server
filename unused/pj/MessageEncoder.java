package com.swiggy.pj;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.websocket.*;
import java.io.IOException;

public class MessageEncoder<T> implements Encoder.Text<T>, Decoder.Text<T> {

    private static ObjectMapper mapper = new ObjectMapper();

    TypeReference<T> ref = new TypeReference<T>() {
    };

    @Override
    public T decode(String s) throws DecodeException {
        try {
            return mapper.readValue(s, ref);
        } catch (IOException e) {
            throw new DecodeException("", "", e);
        }
    }

    @Override
    public boolean willDecode(String s) {
        return true;
    }

    @Override
    public String encode(T t) throws EncodeException {
        try {
            return mapper.writeValueAsString(t);
        } catch (JsonProcessingException e) {
            throw new EncodeException("", "", e);
        }
    }

    @Override
    public void init(EndpointConfig config) {

    }

    @Override
    public void destroy() {

    }
}
