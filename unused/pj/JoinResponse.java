package com.swiggy.pj;

import lombok.Data;

@Data
public class JoinResponse {

    private String msg;

    public JoinResponse(String msg) {
        this.msg = msg;
    }

    public JoinResponse() {
    }
}
