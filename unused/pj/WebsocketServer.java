package com.swiggy.pj;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

//@ServerEndpoint(value = "/join/{roomId}/{userId}",
//        encoders = {CMessageEncoder.class, JoinResponseEncoder.class, JoinMessageEncoder.class},
//        decoders = {CMessageEncoder.class, JoinResponseEncoder.class, JoinMessageEncoder.class})
public class WebsocketServer {

    private AtomicInteger cnt = new AtomicInteger();

    @OnOpen
    public void open(Session session, EndpointConfig c, @PathParam("roomId") String roomId, @PathParam("userId") String userId) {

        Optional<Session> sessional = Optional.of(session);

        try {
            session.getBasicRemote().sendObject(new JoinResponse("Welcome! " + cnt.incrementAndGet()));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (EncodeException e) {
            e.printStackTrace();
        }

    }

    @OnMessage
    public void textMessage(Session session, String msg) {

        try {
            session.getBasicRemote().sendObject(new JoinResponse("Welcome! " + cnt.incrementAndGet()));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (EncodeException e) {
            e.printStackTrace();
        }
        System.out.println("Text message: " + msg);
    }

    @OnError
    public void error(Session session, Throwable error) {

        System.out.println("session closed");

    }

    //    public void send(CMessage msg) throws IOException, EncodeException {
    //        sessional.get().getBasicRemote().sendObject(new CMessage("Custom message!"));
    //    }

}
