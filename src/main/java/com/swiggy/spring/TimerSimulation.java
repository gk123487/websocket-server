package com.swiggy.spring;

import java.util.Random;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TimerSimulation {

    public static void main(String[] args) throws InterruptedException {

        Random random = new Random();

        ScheduledExecutorService ses = new ScheduledThreadPoolExecutor(10);

        for (int i = 0; i < 100; i++) {

            int delay = random.nextInt(1000);
            ScheduledFuture<?> schedule = ses.schedule(task(i, delay), delay, TimeUnit.MILLISECONDS);
            schedule.cancel(false);

        }

        ses.shutdown();
        ses.awaitTermination(1000, TimeUnit.SECONDS);

    }

    private static Runnable task(int id, int delay) {

        return new Runnable() {
            long t = System.currentTimeMillis();

            @Override
            public void run() {

                long l = System.currentTimeMillis();
                System.out.println(id + ".Delay=" + delay + " Diff=" + ((l - t) - delay));
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        };
    }

}
