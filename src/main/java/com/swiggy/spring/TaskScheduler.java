package com.swiggy.spring;

import com.swiggy.spring.entity.UserRoom;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Service
public class TaskScheduler {

    private final ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(10);

    private final Map<UserRoom, ScheduledFuture<?>> futures = new HashMap<>();

    public boolean cancel(UserRoom ur) {
        return this.futures.get(ur).cancel(false);
    }

    public boolean cancel(UserRoom ur, boolean force) {
        return this.futures.get(ur).cancel(force);
    }

    public ScheduledFuture<?> schedule(UserRoom ur, Runnable t, int delay, TimeUnit tu) {
        return this.futures.put(ur, this.executorService.schedule(t, delay, tu));
    }

}
