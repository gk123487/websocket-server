package com.swiggy.spring.socket.auth;

import org.springframework.stereotype.Service;

@Service
public class UserAuthenticationService {

    public AuthResponse authenticate(String sessionId) {

        //authenticate user
        return new AuthResponse(AuthCode.SUCCESS,sessionId);

    }


}
