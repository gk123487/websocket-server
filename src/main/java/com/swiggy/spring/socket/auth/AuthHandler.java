package com.swiggy.spring.socket.auth;

import com.swiggy.spring.socket.Literals;
import com.swiggy.spring.socket.SocketResponseCode;
import com.swiggy.spring.socket.response.ResponseType;
import com.swiggy.spring.socket.response.SocketResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class AuthHandler {

    @Autowired
    private UserAuthenticationService userAuthenticationServie;

    public boolean handle(ServerHttpResponse response, String sessionId) throws IOException {
        //authenticate
        AuthResponse authenticate = userAuthenticationServie.authenticate(sessionId);
        if (authenticate.getAuthCode().equals(AuthCode.SUCCESS)) {
            return true;
        } else if (authenticate.getAuthCode().equals(AuthCode.TIMEDOUT)) {
            response.getHeaders().set(Literals.RESPONSE_TYPE, ResponseType.SessionTimedOut.name());
            response.getBody().write(new SocketResponse<>(SocketResponseCode.SESSION_TIMEDOUT, "Timeout out").jsonBytes());
            return false;
        } else if (authenticate.getAuthCode().equals(AuthCode.INVALID)) {
            response.getHeaders().set(Literals.RESPONSE_TYPE, ResponseType.Invalid.name());
            response.getBody().write(new SocketResponse<>(SocketResponseCode.INVALID_SESSION, "Invalid session").jsonBytes());
            return false;
        }

        return false;
    }

}
