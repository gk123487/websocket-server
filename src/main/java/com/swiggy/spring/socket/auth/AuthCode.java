package com.swiggy.spring.socket.auth;


public enum AuthCode {
    SUCCESS(10001), TIMEDOUT(10002), FAILURE(10003),INVALID(10004);

    public int getCode() {
        return code;
    }

    AuthCode(int code) {
        this.code = code;
    }

    private final int code;


}
