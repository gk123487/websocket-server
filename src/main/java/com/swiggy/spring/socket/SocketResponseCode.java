package com.swiggy.spring.socket;

public class SocketResponseCode {

    public static int SUCCESS = 1000;

    public static int SESSION_TIMEDOUT = 1001;

    public static int INVALID_SESSION = 1002;

}
