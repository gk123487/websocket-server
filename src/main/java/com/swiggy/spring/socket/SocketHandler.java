package com.swiggy.spring.socket;

import com.swiggy.spring.entity.UserRoom;
import com.swiggy.spring.entity.UserRoomMessage;
import com.swiggy.spring.messaging.UpstreamMessageRouter;
import com.swiggy.spring.socket.request.RequestType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Component
public class SocketHandler extends TextWebSocketHandler {

    @Autowired
    public SocketHandler(UpstreamMessageRouter upstreamMessageRouter, SocketHolder socketHolder) {
        this.upstreamMessageRouter = upstreamMessageRouter;
        this.socketHolder = socketHolder;
    }

    private final UpstreamMessageRouter upstreamMessageRouter;
    private final SocketHolder socketHolder;


    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage msg)
            throws IOException {

        //TODO authenticate

//        RequestType requestType = RequestType.valueOf(session.getHandshakeHeaders().get(Literals.REQUEST_TYPE).get(0));

//        try {
//            UserRoomMessage req = (UserRoomMessage) RequestFactory.from(msg.getPayload(), requestType);
//        } catch (Exception ex) {
//            session.sendMessage(new TextMessage("Illegal Message"));
//        }

//        upstreamMessageRouter.send(requestType, );

//        System.out.println(Thread.currentThread().getName());

//        Thread.sleep(100000);
//        throw new IOException();
        session.sendMessage(new TextMessage("sdfgfds"));

    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        UserRoom ur = (UserRoom) session.getAttributes().get("userRoom");
    }

    public void sendMsg(String uid, String rid, String m) throws IOException {


    }

    public void sendMsg(UserRoom ur, String msg, Runnable timeoutAction, int delay,
                        TimeUnit timeUnit) throws IOException {

    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws IOException {
        session.close(status);

//        UserRoom ur = (UserRoom) session.getAttributes().get("userRoom");
//        userRoomDataHolder.remove(ur);

        System.out.println("Connection closed");

    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {

        UserRoom ur = (UserRoom) session.getAttributes().get("userRoom");
        System.err.println("Error " + ur + " " + exception);

    }

}
