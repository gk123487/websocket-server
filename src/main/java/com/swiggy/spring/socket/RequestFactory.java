package com.swiggy.spring.socket;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.spring.socket.request.JoinRoomRequest;
import com.swiggy.spring.socket.request.RequestType;

import java.io.IOException;

public class RequestFactory {

    private static ObjectMapper mapper = new ObjectMapper();

    public static Object from(String s, RequestType t) throws IOException {

        if (RequestType.JoinRoom.equals(t))
            return mapper.readValue(s, JoinRoomRequest.class);
        else throw new IllegalArgumentException();

    }

}
