package com.swiggy.spring.socket;

public class Literals {

    public static final String RESPONSE_TYPE = "ResponseType";
    public static final String REQUEST_TYPE = "RequestType";

}
