package com.swiggy.spring.socket;

import com.swiggy.spring.entity.UserRoom;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class SocketHolder {

    private final Map<UserRoom, WebSocketSession> sessions = new HashMap<>();

    public Optional<WebSocketSession> getSession(UserRoom ur) {
        return Optional.ofNullable(sessions.get(ur));
    }

    public void put(UserRoom ur, WebSocketSession ses) {
        this.sessions.put(ur, ses);
    }

    public void remove(UserRoom ur) {
        this.sessions.remove(ur);
    }

}
