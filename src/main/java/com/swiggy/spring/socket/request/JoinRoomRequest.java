package com.swiggy.spring.socket.request;

import com.swiggy.spring.entity.UserRoomMessage;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class JoinRoomRequest extends UserRoomMessage {

}
