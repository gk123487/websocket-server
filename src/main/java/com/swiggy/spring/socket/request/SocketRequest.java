package com.swiggy.spring.socket.request;

import com.swiggy.spring.socket.request.RequestType;
import lombok.Data;

@Data
public class SocketRequest<T> {

    private RequestType requestType;
    private T data;

}
