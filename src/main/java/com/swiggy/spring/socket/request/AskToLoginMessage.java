package com.swiggy.spring.socket.request;

import lombok.Data;

@Data
public class AskToLoginMessage {

    private int userId;

}
