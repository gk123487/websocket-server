package com.swiggy.spring.socket.request;

public enum RequestType {

    JoinRoom, LeaveRoom, Timeout, FoldAction;

}
