package com.swiggy.spring.socket.response;

import com.swiggy.spring.entity.UserRoomMessage;
import com.swiggy.spring.entity.UserRoom;
import com.swiggy.spring.socket.SocketHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Optional;

@Service
@Slf4j
public class MessageSender {

    @Autowired
    public MessageSender(SocketHolder socketHolder) {
        this.socketHolder = socketHolder;
    }

    private final SocketHolder socketHolder;

    public void send(UserRoomMessage t) {

        Optional<WebSocketSession> session = socketHolder.getSession(new UserRoom(t.getUserId(), t.getRoomId()));
        if (session.isPresent()) {
            if (session.get().isOpen()) {
                try {
                    session.get().sendMessage(new TextMessage(t.toJsonString()));
                } catch (IOException e) {
                    log.error("", e);
                }
            } else
                log.info(String.format("socket is not open for userId %s,roomId %s", t.getUserId(), t.getRoomId()));
        } else {
            //msg to backend?
            log.info(String.format("socket is not available userId %s,roomId %s", t.getUserId(), t.getRoomId()));
        }

    }
}