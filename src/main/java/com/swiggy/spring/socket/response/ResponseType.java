package com.swiggy.spring.socket.response;

public enum ResponseType {

    JoinedRoomSuccessfully, AskUserToLogin, FoldSuccessful, BetSuccess, BadRequest, SessionTimedOut,
    Invalid

}
