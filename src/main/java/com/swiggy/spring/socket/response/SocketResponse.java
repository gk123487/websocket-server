package com.swiggy.spring.socket.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.nio.charset.StandardCharsets;

@Data
public class SocketResponse<T> {

    private static ObjectMapper mapper = new ObjectMapper();

    public SocketResponse() {
    }

    public SocketResponse(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public SocketResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    private int code;
    private String message;
    private T data;
    private ResponseType responseType;

    public String toJson() {
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public byte[] jsonBytes() {
        return toJson().getBytes(StandardCharsets.UTF_8);
    }

}
