package com.swiggy.spring.socket.response;

import com.swiggy.spring.messaging.MessageBroker;
import com.swiggy.spring.socket.response.JoinRoomResponse;
import com.swiggy.spring.socket.response.MessageSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@Slf4j
public class ResponseExecutor {

    private ExecutorService responseSenderPool = Executors.newFixedThreadPool(1);
    private final MessageSender messageSender;

    @Autowired
    public ResponseExecutor(MessageSender messageSender, MessageBroker messageBroker) {
        this.messageSender = messageSender;
        this.messageBroker = messageBroker;
    }

    private final MessageBroker messageBroker;

    @PostConstruct
    public void listen() {

        responseSenderPool.execute(() -> {
            while (true) {
                try {
                    JoinRoomResponse res = messageBroker.listenOnJoinRoom();
                    messageSender.send(res);

                } catch (Exception e) {
                    log.error("Error", e);
                }
            }
        });

    }

}
