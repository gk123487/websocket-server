package com.swiggy.spring.socket.response;

import com.swiggy.spring.entity.UserRoomMessage;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class JoinRoomResponse extends UserRoomMessage {

}
