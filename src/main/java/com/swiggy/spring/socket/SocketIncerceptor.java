package com.swiggy.spring.socket;

import com.swiggy.spring.socket.auth.AuthHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import java.util.Map;

@Service
public class SocketIncerceptor implements HandshakeInterceptor {

    @Autowired
    private AuthHandler authHandler;

    @Override
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
                                   Map<String, Object> attributes) throws Exception {

//        List<String> requestType = request.getHeaders().get(Literals.REQUEST_TYPE);
//        if (requestType == null || requestType.isEmpty()) {
//            response.getHeaders().set(Literals.RESPONSE_TYPE, ResponseType.BadRequest.name());
//            response.getBody().write(new SocketResponse<>(HttpStatus.BAD_REQUEST.value(),
//                    "No RequestType header found").jsonBytes());
//            return false;
//        }
//
//        List<String> sessionData = request.getHeaders().get("SessionId");
//        if (sessionData == null || sessionData.isEmpty()) {
//            response.getHeaders().set(Literals.RESPONSE_TYPE, ResponseType.AskUserToLogin.name());
//            response.getBody().write(new SocketResponse<>(HttpStatus.OK.value(), "Please login").jsonBytes());
//            return false;
//        } else {
//            return authHandler.handle(response, sessionData.get(0));
//        }

        System.out.println("Touched ");
        return true;

    }

    @Override
    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
                               Exception exception) {
    }

}
