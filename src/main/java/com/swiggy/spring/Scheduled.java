package com.swiggy.spring;

import com.swiggy.spring.socket.SocketHandler;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

//@Service
public class Scheduled {

    private ScheduledExecutorService ex = new ScheduledThreadPoolExecutor(1);

    @Autowired
    private SocketHandler socketHandler;

//    @PostConstruct
    public void start() {
        ex.scheduleAtFixedRate(() -> {
            try {
                socketHandler.sendMsg("2", "1", "Hello");
            } catch (IOException e) {
                System.out.println(e);
            }
        }, 0, 3, TimeUnit.SECONDS);

        Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {

            }
        }, 123);
        t.cancel();
    }

}
