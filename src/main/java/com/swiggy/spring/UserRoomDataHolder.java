package com.swiggy.spring;

import com.swiggy.spring.entity.UserRoom;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class UserRoomDataHolder {

    private Map<UserRoom, WebSocketSession> sessions = new ConcurrentHashMap<>();



    public Optional<WebSocketSession> getSession(UserRoom ur) {
        return Optional.ofNullable(sessions.get(ur));
    }

    public void put(UserRoom ur, WebSocketSession ses) {
//        this.sessions.put(ur, ses);
    }

    public void remove(UserRoom ur) {
        this.sessions.remove(ur);
    }

}
