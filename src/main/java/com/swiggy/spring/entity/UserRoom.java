package com.swiggy.spring.entity;

import lombok.Data;

@Data
public class UserRoom {

    private String userId;
    private String roomId;

    public UserRoom(String userId, String roomId) {
        this.userId = userId;
        this.roomId = roomId;
    }

    public UserRoom() {
    }
}
