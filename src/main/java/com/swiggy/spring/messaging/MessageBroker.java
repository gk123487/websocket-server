package com.swiggy.spring.messaging;

import com.swiggy.spring.socket.request.JoinRoomRequest;
import com.swiggy.spring.socket.response.JoinRoomResponse;
import org.springframework.stereotype.Service;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

@Service
public class MessageBroker {

    private final BlockingQueue<JoinRoomRequest> joinRoomRequestQueue = new LinkedBlockingDeque<>();

    private final BlockingQueue<JoinRoomResponse> joinRoomResponseQueue = new LinkedBlockingDeque<>();

    public void publish(JoinRoomRequest msg) {

        this.joinRoomRequestQueue.add(msg);

    }

    public JoinRoomResponse listenOnJoinRoom() throws InterruptedException {

        return joinRoomResponseQueue.take();

    }

}
