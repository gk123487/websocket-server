package com.swiggy.spring.messaging;

import com.swiggy.spring.entity.UserRoomMessage;
import com.swiggy.spring.socket.request.JoinRoomRequest;
import com.swiggy.spring.socket.request.RequestType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpstreamMessageRouter {

    private final MessageBroker messageBroker;

    @Autowired
    public UpstreamMessageRouter(MessageBroker messageBroker) {
        this.messageBroker = messageBroker;
    }

    public void send(RequestType t,UserRoomMessage m) {

        if (t.equals(RequestType.JoinRoom))
            messageBroker.publish((JoinRoomRequest) m);

    }

}
